
module.exports = {
  entry: ['babel-polyfill', './handler.js'],
  target: 'node',
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: __dirname,
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'stage-0']
        }
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  externals: {
    'aws-sdk': 'aws-sdk',
    'google-spreadsheet-to-json': 'google-spreadsheet-to-json'
  }
};

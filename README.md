# Current Models
nxpi (tech) - short-term quotes (1.25 - -0.7). Long term (same?)
c (financial) - short-term quotes (0.74 - -0.73). Long term (0.55 - -0.5)
esl (correlated to) - addicted-sambar Note: increase the down threshold slightly


Creates a forecast model and evaluation using data in trading-features/{klass}/{id}/model

Notes: Don't delete node_modules. The google spreadsheet related modules have some custom code in them related
to how they reference __dirname. Webpack doesn't handle it well. See as an example of the issue:
https://github.com/thereactivestack-legacy/meteor-webpack/issues/238

# Step 1: create the recipe in ./recipe.json;
set period to '14d' for realtime (s3 based, longer term data); 
15d for realtime but short-term quotes; 
'3y' for daily;

If there are removeProperties present, be sure to adjust the defaultRecipe in ./lib/ml and remove the properties.
 
# Step 2: run 'npm run create' (-> ./start.sh);


# Create a handler test:
Add the plugin to serverless.yml:
plugins:
  - serverless-mocha-plugin
sls create test -f setupRulesRealtime --path specs/handlers

# examples
serverless webpack invoke --function prepareModel --path events/prepareModel.json


// Might need to add this permission:
aws lambda add-permission \
--function-name trading-forecast-daily-dev-fetchBatchPredict \
--statement-id ConsoleId \
--action 'lambda:InvokeFunction' \
--principal events.amazonaws.com \
--source-arn arn:aws:events:us-east-1:844412190991:rule/belligerent-polecat-stocks-createModel

// Sample lambda policy with permission for the daily rule
"Policy": {
  "Version":"2012-10-17",
  "Id":"default",
  "Statement":[
    {
      "Sid":"trading-forecast-daily-dev-createBatchPredictScheduleEventPermission0-XH0DKOJGVF2L",
      "Effect":"Allow",
      "Principal":{
        "Service":"events.amazonaws.com"},
        "Action":"lambda:InvokeFunction",
        "Resource":"arn:aws:lambda:us-east-1:844412190991:function:trading-forecast-daily-dev-createBatchPredict",
        "Condition":{
          "ArnLike":{
            "AWS:SourceArn":"arn:aws:events:us-east-1:844412190991:rule/trading-forecast-daily-de-createBatchPredictSchedu-
10NRWWRKEQCDQ"
          }
        }
      },
      {"Sid":"ConsoleId",
       "Effect":"Allow",
       "Principal":{
        "Service":"events.amazonaws.com"},
        "Action":"lambda:InvokeFunction",
        "Resource":"arn:aws:lambda:us-east-1:844412190991:function:trading-forecast-daily-dev-createBatchPredict",
        "Condition":{
          "ArnLike":{
            "AWS:SourceArn":"arn:aws:events:us-east-1:844412190991:rule/belligerent-polecat-stocks-createModel"
          }
        }
      }
    ]
  }"
}

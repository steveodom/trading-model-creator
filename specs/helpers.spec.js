import {expect} from 'chai';
import { determineEntryTrade, meetsFilter } from './../lib/helpers';

describe('Helpers - determineEntryTrade', () => {
  const recipe = {
    "period": "14d",
    "changeCalc": {
      "method": "isGreaterThan",
      "n": 10,
      "threshold1": 0.01,
      "threshold2": -0.5
    },
    "modelType": "BINARY"
  };

  it('should return a string of the kind of trade to enter in to', () => {
    let res = determineEntryTrade(recipe);
    expect(typeof res).to.equal('string');
  });

  it('should return buy if BINARY and isGreaterThan', () => {
    let res = determineEntryTrade(recipe);
    expect(res).to.equal('buy');
  });

  it('should return sell_short if BINARY and isLessThan', () => {
    const modifiedRecipe = Object.assign({}, recipe, {
      changeCalc: {
        method: 'isLessThan'
      }
    });
    let res = determineEntryTrade(modifiedRecipe);
    expect(res).to.equal('sell_short');
  });

  it('should return MULTICLASS if MULTICLASS', () => {
    const modifiedRecipe = Object.assign({}, recipe, {
      modelType: 'MULTICLASS'
    });
    let res = determineEntryTrade(modifiedRecipe);
    expect(res).to.equal('MULTICLASS');
  });
});

describe('Helpers - meetsFilter', () => {
  const ticker = { 
    name: 'INTL BUSINESS MACHINES CORP',
    ticker: 'IBM',
    avgVolume: 3506284,
    mktcap: 164402161046,
    beta: 0.97
  }
  it('should return the appropriate boolean', () => {
    const res = meetsFilter(ticker, {});
    expect(res).to.equal(true);
  });

  it('should return the appropriate boolean', () => {
    const filters = {
      mktcap: {lessThan: 100000000000}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(false);
  });

  it('should return the appropriate boolean', () => {
    const filters = {
      mktcap: {lessThan: 164402161047}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(true);
  });
  
  it('should return the appropriate boolean', () => {
    const filters = {
      mktcap: {lessThan: 164402161047},
      beta: {greaterThan: 1}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(false);
  });

  it('should return the appropriate boolean', () => {
    const filters = {
      mktcap: {lessThan: 164402161047},
      beta: {lessThan: 1}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(true);
  });

  it('should return the appropriate boolean', () => {
    const filters = {
      mktcap: {lessThan: 164402161047, greaterThan: 164402161045},
      beta: {lessThan: 1}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(true);
  });

  it('should return the appropriate boolean', () => {
    // mktcap: 164402161046,
    // beta: 0.97

    const filters = {
      mktcap: {lessThan: 164402161047, greaterThan: 164402161045},
      beta: {greaterThan: 1}, // should fail on this
      missingFilter: {greaterThan: 0}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(false);
  });

  it('should reject data that is a string. Must be a number', () => {
    const modifiedTicker = Object.assign({}, ticker, {
      beta: '0.97'
    })
    const filters = {
      mktcap: {lessThan: 164402161047, greaterThan: 164402161045},
      beta: {lessThan: 1},
      missingFilter: {greaterThan: 0}
    }
    const res = meetsFilter(modifiedTicker, filters);
    expect(res).to.equal(false);
  });

  it('should handle undefined meta data by returning false', () => {
    const modifiedTicker = Object.assign({}, ticker, {
      beta: undefined
    })
    const filters = {
      beta: {lessThan: 1}
    }
    const res = meetsFilter(modifiedTicker, filters);
    expect(res).to.equal(false);
  });

  it('should handle ticker equal to where the ticker is not found', () => {
    const filters = {
      tickers: {equalTo: ['ECL', 'WSO']}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(false);
  });

  it('should handle ticker equal to where the ticker is found', () => {
    const filters = {
      tickers: {equalTo: ['ECL', 'WSO', 'IBM']}
    }
    const res = meetsFilter(ticker, filters);
    expect(res).to.equal(true);
  });
});
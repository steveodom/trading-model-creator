'use strict';

import nock from 'nock';
nock.emitter.on('no match', function(req) {
  console.log(req, 'request');
});

const mod = require('../../handler.js');
const mochaPlugin = require('serverless-mocha-plugin');

const lambdaWrapper = mochaPlugin.lambdaWrapper;
const expect = mochaPlugin.chai.expect;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'setupRulesRealtime' });

import { sandbox } from 'sinon';
import * as cloudwatch from './../../lib/cloudwatch';
import * as fb from 'firebase';
import * as ml from './../../lib/ml';
import * as s3 from './../../lib/s3';
import * as tickers from './../../lib/tickers';

const mockSandbox = sandbox.create();
let mockCloudwatch;
let mockML;

const baseEvent = {
  id:'black-lory', 
  klass: 'stocks',
  modelType: 'BINARY',
  dailyRunTime: '3/15'
}

const recipe = {
    "period": "14d",
    "changeCalc": {
      "method": "isGreaterThan",
      "n": 10,
      "threshold1": 0.01,
      "threshold2": -0.5
    },
    "modelType": "BINARY"
  };

const detailRealtime = { 
    name: 'black-lory-stocks-createRealtime',
    description: 'loops through all the tickers and generates a fetchRealtime request for each',
    lambda: 'arn:aws:lambda:us-east-1:844412190991:function:trading-forecast-daily-dev-createRealTime' 
  };

const detailQueue = { 
  name: 'black-lory-stocks-checkQueue',
  description: 'checks the prediction update queue to initialize adding start and end quotes',
  lambda: 'arn:aws:lambda:us-east-1:844412190991:function:trading-forecast-checker-dev-checkQueue' }

const cron = 'cron(3/15 14-21 ? * MON-FRI *))';

const eventData = {
  period:"1d",
  bucket:"trading-features",
  id:"black-lory",
  klass:"stocks",
  entryType:"buy"
}


describe('setupRulesRealtime', () => {
  before((done) => {
    mockCloudwatch = mockSandbox.mock(cloudwatch);
    mockML = mockSandbox.mock(ml);
    
    mockSandbox.stub(fb, 'fbUpdate');
    mockSandbox.stub(tickers, 'fetchSpreadsheetTickers').returns(Promise.resolve([]));
    mockSandbox.stub(fb, 'saveModelInfo').returns(Promise.resolve({}));
    done();
  });

  after( (done) => {
    mockCloudwatch.restore();
    mockSandbox.restore();
    done();
  });

  it('should cloudwatch rules and targets with right arguments', () => {
    mockSandbox.stub(s3, 'fetch').returns(Promise.resolve(JSON.stringify(recipe)));
    const initCreateRealtimeRule = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailRealtime, JSON.stringify(eventData)).once();
    const initCreateCheckQueue = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailQueue).once();
    const mockedCreateMLEndpoint = mockML.expects('createRealtimeEndpoint').once();
    
    return wrapped.run(baseEvent).then((response) => {
      initCreateRealtimeRule.verify();
      initCreateCheckQueue.verify();
      mockedCreateMLEndpoint.verify();
    });
  });
});


describe('setupRulesRealtime - MULTICLASS', () => {
  const modifiedBaseEvent = Object.assign({}, baseEvent, {
    modelType: 'MULTICLASS'
  })
  
  const modifiedRecipe = Object.assign({}, recipe, {
    modelType: 'MULTICLASS'
  });

  const modifiedEventData = Object.assign({}, eventData, {
    entryType: "MULTICLASS"
  });

  before((done) => {
    mockCloudwatch = mockSandbox.mock(cloudwatch);
    mockML = mockSandbox.mock(ml);
    
    mockSandbox.stub(s3, 'fetch').returns(Promise.resolve(JSON.stringify(modifiedRecipe)));
    mockSandbox.stub(fb, 'fbUpdate');
    mockSandbox.stub(tickers, 'fetchSpreadsheetTickers').returns(Promise.resolve([]));
    mockSandbox.stub(fb, 'saveModelInfo').returns(Promise.resolve({}));
    done();
  });

  after( (done) => {
    mockCloudwatch.restore();
    mockSandbox.restore();
    done();
  });

  it('should cloudwatch rules and targets with right arguments revised', () => {  
    const initCreateRealtimeRule = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailRealtime, JSON.stringify(modifiedEventData)).once();
    const initCreateCheckQueue = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailQueue).once();
    const mockedCreateMLEndpoint = mockML.expects('createRealtimeEndpoint').once();
    
    return wrapped.run(modifiedBaseEvent).then((response) => {
      initCreateRealtimeRule.verify();
      initCreateCheckQueue.verify();
      mockedCreateMLEndpoint.verify();
    });
  });
});


describe('setupRulesRealtime - isLessThan', () => {
  const modifiedRecipe = Object.assign({}, recipe, {
    changeCalc: {
      method: 'isLessThan'
    }
  });

  const modifiedEventData = Object.assign({}, eventData, {
    entryType: "sell_short"
  });

  before((done) => {
    mockCloudwatch = mockSandbox.mock(cloudwatch);
    mockML = mockSandbox.mock(ml);
    
    mockSandbox.stub(s3, 'fetch').returns(Promise.resolve(JSON.stringify(modifiedRecipe)));
    mockSandbox.stub(fb, 'fbUpdate');
    mockSandbox.stub(tickers, 'fetchSpreadsheetTickers').returns(Promise.resolve([]));
    mockSandbox.stub(fb, 'saveModelInfo').returns(Promise.resolve({}));
    done();
  });

  after( (done) => {
    mockCloudwatch.restore();
    mockSandbox.restore();
    done();
  });

  it('should cloudwatch rules and targets with right arguments revised', () => {  
    const initCreateRealtimeRule = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailRealtime, JSON.stringify(modifiedEventData)).once();
    const initCreateCheckQueue = mockCloudwatch.expects('createRuleAndTarget').withArgs(detailQueue).once();
    const mockedCreateMLEndpoint = mockML.expects('createRealtimeEndpoint').once();
    
    return wrapped.run(baseEvent).then((response) => {
      initCreateRealtimeRule.verify();
      initCreateCheckQueue.verify();
      mockedCreateMLEndpoint.verify();
    });
  });
});

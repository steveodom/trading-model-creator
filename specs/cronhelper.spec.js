import {expect} from 'chai';
import { convertTimeToCronDate } from './../lib/cronHelper';

describe('cronHelper - convertTimeToCronDate', () => {
  it('should return an object', () => {
    let res = convertTimeToCronDate('09:15pm');
    expect(typeof res).to.equal('object');
    expect(res).to.have.property('hour');
    expect(res).to.have.property('min');
  });

  
  it('should allow you to spedify an timezone offset', () => {
    let res = convertTimeToCronDate('09:15pm', 0, -5);
    expect(res.hour).to.equal('2');
    expect(res.min).to.equal('15');
  });

  it('should allow you to spedify an timezone offset CST', () => {
    let res = convertTimeToCronDate('09:15pm', 0, -6);
    expect(res.hour).to.equal('3');
  });

  it('should allow you to spedify a minute offset', () => {
    let res = convertTimeToCronDate('09:15pm', 5, -6);
    expect(res.min).to.equal('20');
  });
});
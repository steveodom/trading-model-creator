# July 12 - testing Financial Group
Changes that seem important:
* "useAverageRange": false on matrix chart
* getting the up/down threshold evenly distributed
* using indexDirection to avoid headwind trades.

Testing and comparing these Financial group models: 
1. knowledgable-pintail (balanced change, 14d (long-term), "useAverageRange": false, mxc-1 added);
2. woozy-gopher (balanced change, 14d, "useAverageRange": true, no mxc-1)

First day of trading, knowledgable-pintail had better results.
24 wins 12 losses, 26 avg win, -13 avg loss. $468 total profit. Buys were 7 wins, 4 losses. $94 total.

Woozy-gopher:
30 wins, 27 losses, 38 avg win, -18 avg loss. $673 total profilt. Only 2 buys. The rest shorts.

S&P, Dow, Nasdaq were all down ~.45%.

Goal for July 12.
1. see if results hold up. (2nd day on model)
2. compare short-term quote model lovely-opposum with longer-term.
Lovely-opposum is just like Knowledgable-pintail but 15d instead of 14d.

July 12: 
The tradier prices used for trades were delayed 15 min. So the results above are null.

Updated to the full non-sanboxed Tradier api. Try results today. Going with the 14d (long-term) model. The range of buys and sells was better than the 15d (live-quotes).

July 17 - down market
Running 3 models:
1. belligerent-egret - base
2. secretive-fox - no useIndexDirection
3. colorful-gull - 15d //did not change the threshold to match but it is balanced

Results:
useIndexDirection seems to help. Same amount of wins, but far fewer losses.
belligerent-egret - 27 wins, 20 losses for a $5 profit. The market was down slightly.
One issue was there were a lot of 1 minute trades that don't seem to have stopped out.

Buys did better than shorts (shorts lost $45). Buys were dominated by F.

July 24 - short term (5 min) wasn't working. More losses than wins. Trying 55 minutes and other times again - now with the useIndex to see if that helps.

Running 3 models with different times:
Gigantic-gazer: 30 minutes
material-huron: 60 minutes
sordid-blackbuck: 90 minutes (fewer trades)


Tracking: 
gigantic-gazer: DB 1:49pm short, {
    "down": 0.6991266012191772,
    "down-flat": 0.0899481251835823,
    "flat": 0.0012863484444096684,
    "up": 0.14360544085502625,
    "up-flat": 0.0660334900021553
}

gigantic-gazer: DB 1:24pm buy, {
    "down": 0.2782425582408905,
    "down-flat": 0.08105900883674622,
    "flat": 0.0016320401336997747,
    "up": 0.5691764950752258,
    "up-flat": 0.06988992542028427
}, Result: lost money. went down. up/down ratio: 2x

gigantic-gazer: BK 12:34pm sell-short, {
    "down": 0.43847212195396423,
    "down-flat": 0.09571447968482971,
    "flat": 0.05402015149593353,
    "up": 0.12003551423549652,
    "up-flat": 0.291757732629776
}, Result: made good money. up/down ratio 3.5x

gigantic-gazer: BAC 12:44pm sell-short, {
    "down": 0.43077215552330017,
    "down-flat": 0.1089799553155899,
    "flat": 0.00012930604862049222,
    "up": 0.3590865433216095,
    "up-flat": 0.10103204846382141
}, Result: made good money. up/down ratio: 1.19X, total down/down-flat: 53

gigantic-gazer: CFG 1:04pm buy, {
    "down": 0.19464211165905,
    "down-flat": 0.22945909202098846,
    "flat": 0.001781474449671805,
    "up": 0.42862674593925476,
    "up-flat": 0.14549057185649872
}, Result: lost good money. up/down ratio: 3.5X, buy total: 56

Week of July 25th
July 26th sordid-blackbuck - $380 / -$82 >40 $50 avg win / -$30 avg loss. 73 min duration.
July 27th sordid-blackbuck - $428 / $140 > 40 $83 avg win / -$48 avg loss. 67 min duration.
July 28th sordid-blackbuck - -$806 / -$334 > 40 $26 avg win / $44 avg loss. 64 min duration.

Week of July 30th
90 minute models
July 31 monday - acceptable-crane $218 / $307 >40. $51/avg win. -$31 avg loss
Aug 1 - tues - breakable-stork $-36 / $150 > 40 $34/$24 avg duration 61. (I think this is when I tightened the stop)
Aug 2 - wed - flowering dove -$344 (but lots of missing trades) / $-58 > 40. $26/avg win. -$36 avg loss.
Aug 3 - thurs - UGH shut-argalis - -$520 / -$293 $31/avg win -$34 avg log
Aug 4 - friday shut-argalis- -$167 / $159 > 40. $44 avg win. -$25/avg loss 101 minutes

Total for week: >40 $265. 3 of 5 profitable days. 


Aug 3 two models: 
shut-argalis - 90 minutes.
envious-steenbok - 60 minutes

August 16:
Dynamic stop in place. Figuring out the sensitivity.
model: ecstatic-cobra
Trade analysis:
BAC: short at 10:09. exited due to stop at 10:29. Stock went down slightly, then up slightly above entry price, then proceeded to go down. Could have been a decent profit had the stop would be too tight.

BNS: short at 1:39. exit due to stop at 1:50. Stock mostly went up. It was trending up when we bought it. The 5 period 2StdDev was at 1. The obv was 'up'.

C: short at 1:29. exit at 2:10. locked in profits. wasn't much. missed big move down, literally 2 minutes later. 5stddev was -1.25. obv was up.

DB: short at 1:15. exit at 1:35. was dropping when bought, then bounced back. went down a little; could have locked in a small profit. bounced up and exited out. Continued to go up. 5std was 1.80. obv was down. close-below-avg 5 and 20 were true.

DB buy at 1:55. bought at the peak of a bounce back. started moving down. hit a stop. 5std was 0. obv was down.

GM: short at 12:45. could have caputured some profits before it went up. But tight stop exited out an then it went on a nice run down. std5 = 0.91

GM: short at 1:29. could have caputured some profits. but profit stop not tight enough and it started to move up and eliminated profit. std5 = 0.85

HIG: short at 2:30. signal at the bottom. was trending down quite a bit when purchased. std5 was 0.95. obv was down.

NTRS: short at 2:20. couldn't have locked in some profits with a profit lock.

SCHW: short at 10:10. perfect. could have locked in more profits. std5 0.5.


-- Aug 17th 3 models:
unknown-python: current defaults for stops;
agonizing-dove: "tradeStopProfitTarget": 0.50, "tradeStopScaleRangeBottomMultiplier": 
0.05,
coherent-crake: not linear stop; looser change threshold to generate more trades.

-- August 25th 3: models

flash-curlew: had "exitOnTimeTrigger" set to true. And "tradeStopMinProfitBeforeTighten": 0.15. It was meant to have looser stops and exit on time. But doesnt.

lost $8 on 19 trades (5 trades didn't complete). 5 wins, 7 losses. Avg win $16, loss $13. avg time 36 min. Lower duration than toothsome-cormant.?

toothsome-cormant: gained $26 on 10 trades. 6 wins, 4 losess. avg win $23, avg loss $28. avg time 67 minutes.


#!/bin/sh

echo 'Preparing to update stock quotes';
echo "Current time : $(date +"%T")"

echo 'Step 1. copy tickers over from ticker spreadsheet'
serverless webpack invoke --function prepareModel --path events/prepareModel.json

echo 'Delaying two minutes before building an ML model. Waiting for quotes to all finish'
echo "Current time : $(date +"%T")"
sleep 120
echo 'Step 3. Start building the model using our new features'
echo "Current time : $(date +"%T")"
serverless webpack invoke --function buildModel --path events/buildModel.json

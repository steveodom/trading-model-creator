import Promise from 'bluebird';
import BaseHandler from './Base';
import {name} from 'namer';
import { saveModelInfo } from 'firebase';
import {write} from './../s3';
import recipe from './../../recipe.json';
import jsonfile from 'jsonfile';
import {fetchMetaTickers} from './../tickers';
import { meetsFilter } from './../helpers';
import {initWriteHistory} from './../sns';


export default class PrepareModel extends BaseHandler {
  constructor(rawEvents, cb) {
    super(rawEvents, cb);
    // override base namer with name
    this.namer = name(this.events.klass);
    this.filteredTickers = [];

  }

  checkArguments() {
    const {klass} = this.events;
    if (!klass) this.err = 'klass param is needed';
  }

  writeRecipe() {
    const {directories} = this.namer;
    return write('trading-features', `${directories.recipes}/index.json`, JSON.stringify(recipe));
  }

  writeBuildModel() {
    const {klass} = this.events;
    const {id} = this.namer;
    const buildModel = {
      id,
      klass: klass,
      modelType: recipe.modelType,
      dailyRunTime: recipe.dailyRunTime
    }
    return jsonfile.writeFile('events/buildModel.json', buildModel);
  }

  fetchTickers() {
    return fetchMetaTickers();
  }

  triggerIndividualTickers(tickers) {
    let triggerSchema = true;
    const {klass} = this.events;
    const {id} = this.namer;
    return Promise.each(tickers, (ticker, i) => {
      if (meetsFilter(ticker, recipe.filters)) {
        this.filteredTickers.push(ticker.ticker);

        if (this.filteredTickers.length > 1) triggerSchema = false;
        return Promise.delay(300).return(initWriteHistory(
          ticker.ticker, 
          id, 
          klass, 
          recipe.period, 
          triggerSchema,
          recipe.multiplyFeatures
        ));
      } else {
        return Promise.resolve();
      }
    });
  }

  writeRecipeLocally() {
    const {id} = this.namer;
    return new Promise( function(resolve) {
      return resolve(jsonfile.writeFile(`../recipes/${id}.json`, recipe));
    })
  }

  saveModelToFirebase() {
    const {id} = this.namer;
    return saveModelInfo(`ml-${id}`, this.filteredTickers, recipe);
  }
}
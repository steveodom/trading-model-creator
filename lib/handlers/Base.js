import Promise from 'bluebird';
import { details } from 'namer';
import { closeFirebase } from 'firebase';
import { handleTopicArgs } from './../utils/helpers';



export default class BaseHandler {
  constructor(rawEvents, cb) {
    this.events = handleTopicArgs(rawEvents);
    this.cb = cb;
    this.err = null;
    this.namer = details(this.events.id, this.events.klass);
    this.log(this.events, 'event')
  }

  log() {
    console.log(Object.values(arguments))
  }

  checkArguments() {
    // local class implements this
    // const {klass, id, entryType} = this.events;
    // if (!klass) this.err = 'klass param is needed';
  }

  hasValidArguments() {
    this.checkArguments();
    if (this.err) {
      return Promise.reject(this.err);
    } else {
      return Promise.resolve(true);
    }
  }

  cleanup() {
    closeFirebase();
    this.cb(null, 'completed');
  }

  handleError(err) {
    console.log('is it an error..');
    closeFirebase();
    this.log(err, 'err')
    this.cb(err);
  }
}
import BaseHandler from './Base';
import {createTrainingData, createEvaluationData, createModel, createEvaluation} from './../ml';

export default class BuildModel extends BaseHandler {
  constructor(rawEvents, cb) {
    super(rawEvents, cb);
  }

  checkArguments() {
    const {klass} = this.events;
    if (!klass) this.err = 'klass param is needed';
  }

  trainingData() {
    return createTrainingData(this.namer);
  }

  evaluationData() {
    return createEvaluationData(this.namer);
  }
  
  model() {
    let {modelType} = this.events;
    return createModel(this.namer, modelType);
  }

  evaluation() {
    return createEvaluation(this.namer);
  }
}
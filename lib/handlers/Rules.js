import Promise from 'bluebird';
import BaseHandler from './Base';
import {fetch} from './../s3';
import { determineEntryTrade } from './../utils/helpers';
import {createRuleAndTarget, realtimeTime, deleteRuleAndTarget} from './../cloudwatch';
import {deleteRealtimeEndpoint} from './../ml';
import {fbUpdate } from 'firebase';


export default class ManageEndpoint extends BaseHandler {
  constructor(rawEvents, cb) {
    super(rawEvents, cb);
    this.recipe = {};
    this.entryType = null;
    this.eventObject = {}; //the json that is passed to each lambda
  }

  checkArguments() {
    const {klass, id} = this.events;
    if (!klass) this.err = 'klass param is needed';
    if (!id) this.err = 'id param is needed';
  }

  fetchRecipe() {
    const {directories} = this.namer;
    return fetch('trading-features', `${directories.recipes}/index.json`);
  }

  setEventObject(res) {
    const {id, klass, entryType} = this.events;
    this.recipe = JSON.parse(res);
    this.entryType = determineEntryTrade(this.recipe);
    this.eventObject = JSON.stringify({ 
      period: '1d', 
      bucket: 'trading-features', 
      id, 
      klass, 
      entryType: this.entryType, 
      instructions: JSON.stringify(this.recipe.instructions),
      minutes: this.recipe.changeCalc.n * 5
    });
    return Promise.resolve({});
  }

  setFetchPredictions() {
    const action = this.namer.rules.createRealtime;
    const cronValue = realtimeTime(this.events.dailyRunTime, 0, 0, '14-18');
    return createRuleAndTarget(action, this.eventObject, cronValue);
  }

  deleteFetchPredictions() {
    const action = this.namer.rules.createRealtime;
    return deleteRuleAndTarget(action);
  }

  setCheckQueue() {
    const action = this.namer.rules.checkQueue;
    const cronValue = realtimeTime(this.events.dailyRunTime, 5, 1, '14-19');
    return createRuleAndTarget(action, this.eventObject, cronValue);
  }

  deleteCheckQueue() {
    const action = this.namer.rules.checkQueue;
    return deleteRuleAndTarget(action);
  }

  setDailyEndpointEngagement() {
    const action = this.namer.rules.dailyEngageEndpoint;
    const eventObject = JSON.stringify({ id: this.events.id, klass: 'stocks'});
    const cronValue = `cron(${action.cron})`;
    return createRuleAndTarget(action, eventObject, cronValue);
  }

  setDailyEndpointDisengagement() {
    const action = this.namer.rules.dailyDisengageEndpoint;
    const eventObject = JSON.stringify({ id: this.events.id, klass: 'stocks'});
    const cronValue = `cron(${action.cron})`;
    return createRuleAndTarget(action, eventObject, cronValue);
  }

  deleteDailyEndpointEngagement() {
    const action = this.namer.rules.dailyEngageEndpoint;
    return deleteRuleAndTarget(action);
  }

  deleteDailyEndpointDisengagement() {
    const action = this.namer.rules.dailyDisengageEndpoint;
    return deleteRuleAndTarget(action);
  }

  turnOffEndpoint() {
    return deleteRealtimeEndpoint(this.namer);
  }
  addModelToList() {
    const {id} = this.events;
    return fbUpdate('models', {[id]:  {tags: this.recipe.tags || []}});
  }

  setModelToInactive() {
    const {id} = this.events;
    return fbUpdate(`models/${id}/tags`, {1: 'inactive'});
  }
}
var jsonfile = require('jsonfile');
import {fbUtility} from 'firebase';

export const writeModelEvents = (obj, i) => {
  jsonfile.writeFile('events/model/tickers-' + i + '.json', obj);
}


export const fetchMetaTickers = () =>  {
  return fbUtility.fetch('trading','tickerMeta').then( (obj) => {
    return Object.keys(obj).map( (ticker) => {
      const val = obj[ticker];
      return Object.assign({}, val, {ticker})
    });
  });
};

import {SNS} from 'aws-sdk';

export const initWriteHistory = (ticker, id, klass='stocks', period='3y', triggerSchema=false, multiplyFeatures=false) => {
  const attrs = {
    ticker: {
      DataType: 'String',
      StringValue: ticker
    },
		id: {
      DataType: 'String',
      StringValue: id
    },
		klass: {
			DataType: 'String',
      StringValue: klass
		},
    purpose: {
      DataType: 'String',
      StringValue: 'model'
    },
		period: {
			DataType: 'String',
      StringValue: period
		},
		triggerSchema: {
			DataType: 'String',
      StringValue: triggerSchema.toString()
		},
		multiplyFeatures: {
			DataType: 'String',
      StringValue: multiplyFeatures.toString()
		}
  };
  return notify('arn:aws:sns:us-east-1:844412190991:NotifyWriteHistory', attrs, `writeHistory for ${ticker}`);
}

export const notify = (arn, attrs, msg) => {
	return new Promise((resolve, reject) => {
		const sns = new SNS({params: {TopicArn: arn}, region: 'us-east-1'});

		const params = {
			Message: msg,
			MessageAttributes: attrs
		};
		sns.publish(params, (err, data) => {
			if (err) {
				console.info('there was an error publishing..', err);
				reject(err);
			} else {
				console.info('params', params);
				resolve(`Successfully published`);
			}
		});
	})
	.catch( (err) => {
		console.info('an error occurred', err);
	});
};
import moment from 'moment';

export const convertTimeToCronDate = (timeString = '09:15pm', minuteOffset = 0, offSet = -6) => {
  const date = moment(timeString, 'HH:mmA');
  const base = date.utcOffset(offSet, true).add(minuteOffset, 'm').utc();
  const hour = base.format('h');
  const min = base.format('mm');
  return {hour, min};
}
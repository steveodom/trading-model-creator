import AWS from 'aws-sdk';
import {convertTimeToCronDate } from './cronHelper';

const cloudwatchevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07', region: 'us-east-1'});

export const dailyTime = (dailyRunTime, runTimeOffset) => {
  const timeComponents = convertTimeToCronDate(dailyRunTime, runTimeOffset);
  return `cron(${timeComponents.min} ${timeComponents.hour} ? * MON-FRI *)`;
}

export const realtimeTime = (dailyRunTime, runTimeOffset, minuteFrequencyOverride, hourRange = '14-18') => {
  // 4/15 13-21 ? * MON-FRI *
  // every 15 minutes, on the 4.

  // hourRange:
  // 13-20 is time of day in UTC. Equates to 9:00am EST and 4:00EST
  // we stop at 18 because it includes all of 18 to get us to an hour before close.

  const timeComponents = dailyRunTime.split('/');
  const whatMinute = parseInt(timeComponents[0]) + parseInt(runTimeOffset);
  const minuteFrequency = minuteFrequencyOverride || timeComponents[1];
  return `cron(${whatMinute}/${minuteFrequency} ${hourRange} ? * MON-FRI *)`;
} 

export const createRuleAndTarget = async (details, data, cronTime) => {
  const rule = await createRule(details, cronTime);
  const target = await createTarget(details, data);
  return {rule, target};
}

export const deleteRuleAndTarget = async (details) => {
  const target = await deleteTarget(details);
  const rule = await deleteRule(details);
  return deleteTarget(details).then(deleteRule(details)).then( () => {
    return Promise.resolve();
  });
}

export const createRule = (details, cronTime) => {
  return new Promise((resolve, reject) => {
    const params = {
      Name: details.name,
      Description: details.description,
      RoleArn: 'arn:aws:iam::844412190991:role/trading-dev-IamRoleLambda-PRZS9CNW6JUK',
      ScheduleExpression: cronTime,
      State: 'ENABLED'
    };

    cloudwatchevents.putRule(params, (err, data) => {
      if (err) {
        console.info('error in cloudwatch')
        reject(err);
      } else {
        console.info('createRule was successful')
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
};

export const deleteRule = (details) => {
  return new Promise((resolve, reject) => {
    const params = {
      Name: details.name
    };

    cloudwatchevents.deleteRule(params, (err, data) => {
      if (err) {
        console.info('error in cloudwatch')
        reject(err);
      } else {
        console.info(`deleteRule was successful ${details.name}`)
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
};


// Targets are the data to use in the rule
export const createTarget = (details, data) => {
  return new Promise((resolve, reject) => {
    const params = {
      Rule: details.name,
      Targets: [
        {
          Arn: details.lambda,
          Id: details.name + '-target', /* required */
          Input: data
        },
      ]
    };

    cloudwatchevents.putTargets(params, (err, data) => {
      if (err) {
        console.info('createTarget encountered an error');
        reject(err);
      } else {
        console.info('createTarget was successful');
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}

export const deleteTarget = (details) => {
  return new Promise((resolve, reject) => {
    const params = {
      Ids: [details.name + '-target'],
      Rule: details.name, 
    };

    cloudwatchevents.removeTargets(params, (err, data) => {
      if (err) {
        console.info('error in cloudwatch')
        reject(err);
      } else {
        console.info(`deleteTarget was successful ${details.name}`)
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
};
import AWS from 'aws-sdk';
import recipe from './../recipe.json';
import {buildAwsRecipeGroups} from 'trading-feature-creator'; 

const machinelearning = new AWS.MachineLearning({apiVersion: '2014-12-12', region: 'us-east-1'});

export const dataSourceName = (namer, klass = 'training') => {
  return `${namer.DatasourceIdBase}-${klass}`;
}

export const createTrainingData = (namer) => {
  const id = namer.TrainingDataId;
  const name = namer.TrainingDataName;
  const dataRearrangement = {"splitting":{"percentBegin":0, "percentEnd":70, "strategy":"random", "complement": true}};
  return createDataSource(namer, id, name, dataRearrangement);
}

export const createEvaluationData = (namer) => {
  const id = namer.EvaluationDataId;
  const name = namer.EvaluationDataName;
  const dataRearrangement = {"splitting":{"percentBegin":0, "percentEnd":70, "strategy":"random"}};
  return createDataSource(namer, id, name, dataRearrangement);
}

export const createDataSource = (namer, id, name, dataRearrangement) => {
  return new Promise((resolve, reject) => {
    const params = {
      DataSourceId: id,
      DataSpec: { /* required */
        DataLocationS3: `s3://trading-features/${namer.directories.modelTraining}/`,
        DataSchemaLocationS3: `s3://trading-features/${namer.directories.modelTraining}/.schema`,
        DataRearrangement: JSON.stringify(dataRearrangement)
      },
      DataSourceName: name,      
      ComputeStatistics: true
    };
    machinelearning.createDataSourceFromS3(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}


export const buildRecipe = (recipe) => {
  const {normalize, quantile, ngram} = buildAwsRecipeGroups(recipe);
  const res = {
    "groups": {
      "QUANTILE_TARGETS": quantile,
      "NORMALIZE_TARGETS": normalize,
      "NGRAM_TARGETS": ngram
    },
    assignments: {},
    "outputs": [
      "ALL_BINARY",
      "quantile_bin(QUANTILE_TARGETS, 10)",
      "normalize(NORMALIZE_TARGETS)",
      "ngram(NGRAM_TARGETS, 4)"
    ]
  }
  return JSON.stringify(res);
}

// modelType can be: REGRESSION | BINARY | MULTICLASS
export const createModel = (namer, modelType = 'BINARY') => {  
  const trainingRecipe = buildRecipe(recipe);
  const {maxPasses, regularlizationType, regularlizationAmount, modelSize, shuffleType} = recipe.modelParameters;
  return new Promise((resolve, reject) => {
    const params = {
      TrainingDataSourceId: namer.TrainingDataId,
      MLModelId: namer.MLModelId,
      MLModelName: namer.MLModelName,
      MLModelType: modelType,
      Recipe: trainingRecipe,
      Parameters: {
        'sgd.maxPasses': maxPasses,
        [`sgd.${regularlizationType}RegularizationAmount`]: regularlizationAmount,
        'sgd.maxMLModelSizeInBytes': modelSize,
        'sgd.shuffleType': shuffleType || 'none'
      }
    };

    machinelearning.createMLModel(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}


export const createEvaluation = (namer) => {
  return new Promise((resolve, reject) => {
    const params = {
      EvaluationDataSourceId: namer.EvaluationDataId,
      EvaluationId: namer.EvaluationId,
      MLModelId: namer.MLModelId,
      EvaluationName: namer.EvaluationName
    };

    machinelearning.createEvaluation(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}

export const createRealtimeEndpoint = (namer) => {
  return new Promise((resolve, reject) => {
    const params = {
      MLModelId: namer.MLModelId 
    };

    machinelearning.createRealtimeEndpoint(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}

export const deleteRealtimeEndpoint = (namer) => {
  return new Promise((resolve, reject) => {
    const params = {
      MLModelId: namer.MLModelId 
    };

    machinelearning.deleteRealtimeEndpoint(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}

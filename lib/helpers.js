import _ from 'lodash';
export const determineEntryTrade = (recipe) => {
  const {modelType, changeCalc: {method}} = recipe;
  if (modelType === 'MULTICLASS') {
    return modelType;
  } else {
    if (method === 'isLessThan') return 'sell_short';
    return 'buy'
  }
};

export const meetsFilter = (ticker, filters) =>  {
  let meetsCriteria = [];
  if (_.isEmpty(filters)) return true;
  Object.keys(filters).forEach( (key) => {
    const criteria = filters[key];
    const target = ticker[key];
    
    if (target) {
      Object.keys(criteria).forEach( (k) => {
        const val = criteria[k];
        switch (k) {
        case 'lessThan':
          meetsCriteria.push(target < val);
          break;
        case 'greaterThan':
          meetsCriteria.push(target > val);
          break;
        default:
          break;
        }
      });
    } else {
      if (key === 'tickers') {
        if (criteria.hasOwnProperty('equalTo') && criteria.equalTo.includes(ticker.ticker)) {
          meetsCriteria.push(true);
        } else {
          meetsCriteria.push(false);  
        }
      } else {
        // target was undefined or not found.
        meetsCriteria.push(false);
      }
    }
  });
  return !meetsCriteria.includes(false);
};


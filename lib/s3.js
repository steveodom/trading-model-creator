import AWS from 'aws-sdk';

const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

export const write = (Bucket, Key, Body) => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket,
      Key,
      Body
    };

    s3.putObject(params, (err, data) => {
      if (err) {
        console.info('there was an error writing..', err);
        reject(err);
      } else {
        resolve(`Successfully uploaded data to ${Bucket}/${Key}`);
      }
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}

export const fetch = (Bucket, Key, options = {}) => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket,
      Key
    };

    s3.getObject(params, (err, data) => {
      if (err) {
        reject(err);
      } else{
        resolve(data.Body);
      }
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
};
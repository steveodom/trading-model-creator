import _ from 'lodash';

export const handleTopicArgs = (event) => {
  if (event && event.Records) {
    const params = event.Records[0].Sns.MessageAttributes;
    const obj = {};
    Object.keys(params).forEach( (key) => {
      obj[key] = params[key].Value;    
    });
    return obj;
  } else {
    return event;
  }
}

export const determineEntryTrade = (recipe) => {
  const {modelType, changeCalc: {method}} = recipe;
  if (modelType === 'MULTICLASS') {
    return modelType;
  } else {
    if (method === 'isLessThan') return 'sell_short';
    return 'buy'
  }
};


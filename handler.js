'use strict';

import Promise from 'bluebird';
import Rules from './lib/handlers/Rules';
import PrepareModel from './lib/handlers/PrepareModel';
import BuildModel from './lib/handlers/BuildModel';
import {details} from 'namer';
import _ from 'lodash';
import {fetch} from './lib/s3';

import {createRuleAndTarget, dailyTime, deleteRuleAndTarget} from './lib/cloudwatch';
import { determineEntryTrade} from './lib/helpers';
import {closeFirebase, fbUpdate } from 'firebase';


// Step 1: create the recipe in ./recipe.json;
// Step 2: run 'npm run create' (-> ./start.sh);
// change these in the recipe to override
const baseOptions = {
  spreadsheetId: '1aJDWIJbc1-EvIhm-vCQjXXDMaPEIipfX4FBSBEEA3Ew',
  worksheet: 'highVolume',
  bucket: 'trading-features',
  period: '3y'
};

module.exports.buildModel = (rawEvent, context, cb) => {
  const actions = new BuildModel(rawEvent, cb);
  actions.hasValidArguments()
  .then( () => (actions.trainingData()))  
  .then( () => (actions.evaluationData()))
  .then( () => (actions.model()))
  .then( () => (actions.evaluation()))
  .then( () => (actions.cleanup() ))
  .catch((err) => (actions.handleError(err) ))
};

module.exports.prepareModel = (rawEvent, context, cb) => {
  const actions = new PrepareModel(rawEvent, cb);
  actions.hasValidArguments()
  .then( ()         => (actions.writeRecipe()))  
  .then( ()         => (actions.writeBuildModel()))
  .then( ()         => (actions.fetchTickers()))
  .then( (tickers)  => (actions.triggerIndividualTickers(tickers)))
  .then( ()         => (actions.writeRecipeLocally())
  .then( ()         => (actions.saveModelToFirebase())))
  .then( ()         => (actions.cleanup() ))
  .catch((err)      => (actions.handleError(err) ))
};


// Finalize the model. Sets up cron jobs to monitor the model.
module.exports.setupRules = (event, context, cb) => {
  
  const {id, klass, dailyRunTime} = event;
  if (!event.id || !event.klass) return cb('invalid params');
  
  const namer = details(id, klass);
  let detail = namer.rules.updateQuotes;
  const {directories} = namer;
  
  let entryType;
  let cron;
  let data;

  fetch('trading-features', `${directories.recipes}/index.json`).then( (res) => {
    let recipe = JSON.parse(res);
    entryType = determineEntryTrade(recipe);
    // return fetchSpreadsheetTickers(recipe, config);
    fbUpdate('models', {[id]:  {tags: recipe.tags || []}});
  }).then( () => {
    data = JSON.stringify({ period: '1m', bucket: 'trading-features', id, klass, entryType });
    cron = dailyTime(dailyRunTime, 0);
    createRuleAndTarget(detail, data, cron);
  }).then( (res) => {
    detail = namer.rules.createBatch;
    cron = dailyTime(dailyRunTime, 10);
    return createRuleAndTarget(detail, data, cron);
  })
  .then( (res) => {
    detail = namer.rules.fetchBatch;
    cron = dailyTime(dailyRunTime, 20);
    return createRuleAndTarget(detail, data, cron);
  })
  .then( (res) => {
    detail = namer.rules.checkQueue;
    cron = dailyTime(dailyRunTime, 30);
    return createRuleAndTarget(detail, JSON.stringify({id: id}), cron);
  })
  .then( (res) => {
    closeFirebase();
    cb(null, 'Finished setting model up for trading');
  })
  .catch( (err) => {
    closeFirebase();
    console.info(err, 'err is here');
    cb(err)
  });
};

module.exports.setupRulesRealtime = (rawEvent, context, cb) => {
  const actions = new Rules(rawEvent, cb);
  actions.hasValidArguments()
  .then( () => (actions.fetchRecipe()))  
  .then( (res) => (actions.setEventObject(res)))
  .then( () => (actions.setFetchPredictions()))
  .then( () => (actions.setCheckQueue()))
  .then( () => (actions.setDailyEndpointEngagement()))
  .then( () => (actions.setDailyEndpointDisengagement()))
  .then( () => (actions.addModelToList()))
  .then( () => (actions.cleanup() ))
  .catch((err) => (actions.handleError(err) ))
};

module.exports.deleteRules = (event, context, cb) => {
  const {id, klass} = event;
  if (!event.id || !event.klass) return cb('invalid params');
  
  const namer = details(id, klass);
  let detail = namer.rules.updateQuotes;
  
  let deleteQuotes = deleteRuleAndTarget(detail);
  deleteQuotes.then( (res) => {
    detail = namer.rules.createBatch;
    return deleteRuleAndTarget(detail);
  })
  .then( (res) => {
    detail = namer.rules.fetchBatch;
    return deleteRuleAndTarget(detail);
  })
  .then( (res) => {
    detail = namer.rules.checkQueue;
    return deleteRuleAndTarget(detail);
  })
  .then( (res) => {
    cb(null, 'Finished setting model up for trading');
  })
  .catch( (err) => {
    console.info(err, 'err is here');
    cb(err)
  });
};

module.exports.deleteRealtimeRules = (rawEvent, context, cb) => {
  const actions = new Rules(rawEvent, cb);
  actions.hasValidArguments()
  .then( () => (actions.deleteFetchPredictions()))
  .then( () => (actions.deleteCheckQueue()))
  .then( () => (actions.turnOffEndpoint()))
  .then( () => (actions.deleteDailyEndpointEngagement()))
  .then( () => (actions.deleteDailyEndpointDisengagement()))
  .then( () => (actions.setModelToInactive()))
  .then( () => (actions.cleanup() ))
  .catch((err) => (actions.handleError(err) ));
};

